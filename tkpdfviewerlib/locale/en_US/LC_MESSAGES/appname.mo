��          �      �        E   	     O     W     ]     m     t     �     �     �     �     �     �     �     �     �     �  F   �     /     6  �   :  �   �  �   �  �  �  E        b     j     p     �     �     �     �     �     �     �     �     �     �     �     �  F   �     B     I  �   M  �   �  �   �	                                
                                           	                             A new version of {app_name} is available.
Do you want to download it? APPDESC About About {appname} Cancel Check for updates on startup. Close Help Information Language License No Ok Open Please report this bug on  Save As The language setting will take effect after restarting the application Update Yes You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. {appname} is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. {appname} is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: Juliette Monsel <j_4321@protonmail.com>
POT-Creation-Date: 2018-11-07 16:19+0100
PO-Revision-Date: 2018-11-07 16:23+0100
Last-Translator:  <j_4321@protonmail.com>
Language-Team: English
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 A new version of {app_name} is available.
Do you want to download it? APPDESC About About {appname} Cancel Check for updates on startup. Close Help Information Language License No Ok Open Please report this bug on  Save As The language setting will take effect after restarting the application Update Yes You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/. {appname} is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. {appname} is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. 