# -*- coding: utf-8 -*-
"""
tkPDFViewer - Basic PDF viewer
Copyright 2020 Juliette Monsel <j_4321@protonmail.com>

tkPDFViewer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tkPDFViewer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


ToggleRadiobutton
"""


from tkinter import StringVar


class ToggleVar(StringVar):
    """
    StringVar that is reset to ""  if set twice in a row with the same value

    The goal is to implement a Toggle button which would behave both like a
    checkbutton and a radiobutton.
    """

    def __init__(self, master, value=None, name=None, deselectcmd=lambda: None):
        StringVar.__init__(self, master, value, name)
        self.master = master
        self.deselectcmd = deselectcmd
        self._prev_value = self.get()
        self.trace_add("write", self._trace)

    def _trace(self, *args):
        current_value = self.get()
        if not current_value:
            self.deselectcmd()
        if current_value and current_value == self._prev_value:
            self.master.after(1, self.set, "")
            self._prev_value = ""
        else:
            self._prev_value = current_value
        self._prev_value = current_value
