# -*- coding:Utf-8 -*-
"""
tkPDFViewer - Basic PDF viewer
Copyright 2018 Juliette Monsel <j_4321@protonmail.com>

tkPDFViewer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tkPDFViewer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Custom tkinter messageboxes
"""


from webbrowser import open as url_open
import tkinter as tk
from tkinter import ttk
from .autoscrollbar import AutoScrollbar as Scrollbar
from .constants import ICONS, REPORT_URL


class BaseButtonBox(tk.Toplevel):
    """Base class for message boxes."""

    def __init__(self, parent, *buttons, title="", message="",
                 image=None):
        """
        Create a messagebox with N buttons.

        Arguments:
            parent: parent of the toplevel window
            title: message box title
            message: message box text
            button1/2/3: message displayed on the first/second button
            image: image displayed at the left of the message

        self.result contains the text of the clicked button, "" otherwise
        """

        tk.Toplevel.__init__(self, parent, padx=4, pady=4)
        self.transient(parent)
        self.resizable(False, False)
        self.title(title)
        #~self.columnconfigure(0, weight=1)
        self.result = ""

        self.frame_msg = ttk.Frame(self)
        self.frame_btns = ttk.Frame(self)

        self.create_buttons(*buttons)
        self.create_message(message, image)

        #~self.frame_msg.grid(row=0, column=0, sticky="ewsn")
        #~self.frame_btns.grid(row=2, column=0, sticky="ew")  # one row gap to allow for extra content

        self.frame_btns.pack(side='bottom')
        self.frame_msg.pack(fill='both', side='top')

        try:
            self.grab_set()
        except tk.TclError:
            pass

    def create_message(self, message, image):
        if image in ICONS:
            image = f"::tk::icons::{image}"

        if image:
            ttk.Label(self.frame_msg, image=image).pack(side="left", padx=(10, 4), pady=(10, 4))
        ttk.Label(self.frame_msg, text=message, font="TkDefaultFont 10 bold",
                  wraplength=335).pack(side="left", padx=(4, 10), pady=(10, 4))

    def create_buttons(self, *buttons):
        if not buttons:
            return
        self.frame_btns = ttk.Frame(self)

        if len(buttons) == 1:
            pack_options = dict(padx=8, pady=8)
        elif len(buttons) == 1:
            pack_options = dict(side='left', padx=8, pady=8)
        else:
            pack_options = dict(side='left', fill='x', expand=True, padx=8, pady=8)
        b1 = ttk.Button(self.frame_btns, text=buttons[0], command=lambda: self.command(buttons[0]))
        b1.pack(**pack_options)
        for btn in buttons[1:]:
            ttk.Button(self.frame_btns, text=btn,
                       command=lambda txt=btn: self.command(txt)).pack(**pack_options)
        b1.focus_set()

    def command(self, name):
        self.result = name
        self.destroy()

    def get_result(self):
        return self.result


class OneButtonBox(BaseButtonBox):
    def __init__(self, parent=None, title="", message="", button=_("Ok"), image=None):
        """
        Create a message box with one button and copy/pastable message.

        Arguments:
            parent: parent of the toplevel window
            title: message box title
            message: message box text (that can be selected)
            button: message displayed on the button
            image: image displayed at the left of the message, either a PhotoImage or a string
        """
        BaseButtonBox.__init__(self, parent, button, title=title, message=message, image=image)

    def create_message(self, message, image):
        if image in ICONS:
            image = f"::tk::icons::{image}"
        elif isinstance(image, str):
            self.img = tk.PhotoImage(master=self, file=image)
            image = self.img
        self.frame_msg.rowconfigure(0, weight=1)
        self.frame_msg.columnconfigure(1, weight=1)
        l = len(message)
        w = max(1, min(l, 50))
        h = 0
        for line in message.splitlines():
            h += 1 + len(line) // w
        if h < 3:
            w = min(l, 35)
            h = 0
            for line in message.splitlines():
                h += 1 + len(line) // w
        display = tk.Text(self.frame_msg, relief='flat', highlightthickness=0,
                          font="TkDefaultFont 10 bold", bg=self.cget('bg'),
                          height=h, width=w, wrap="word")
        display.configure(inactiveselectbackground=display.cget("selectbackground"))
        display.insert("1.0", message)
        display.configure(state="disabled")
        display.grid(row=0, column=1, pady=(10, 4), padx=4, sticky="ewns")
        display.bind("<Button-1>", lambda event: display.focus_set())
        if image:
            ttk.Label(self.frame_msg, image=image).grid(row=0, column=0, padx=4, pady=(10, 4))


class ShowError(OneButtonBox):
    def __init__(self, parent=None, title="", message="", traceback="",
                 report_msg=False, button=_("Ok"), image="error"):
        """
        Create an error messagebox.
        Arguments:
            parent: parent of the toplevel window
            title: message box title
            message: message box text (that can be selected)
            button: message displayed on the button
            traceback: error traceback to display below the error message
            report_msg: if True display a suggestion to report error
            image: image displayed at the left of the message, either a PhotoImage or a string
        """
        OneButtonBox.__init__(self, parent, button=button, title=title,
                              message=message, image=image)

        style = ttk.Style(self)
        style.configure("url.TLabel", foreground="blue")
        style.configure("txt.TFrame", background='white')
        if not parent:
            style.theme_use('clam')

        if traceback:
            self.frame_msg.pack(fill='x', side='top')
            self.frame_traceback = ttk.Frame(self)
            self.create_traceback(message, traceback)
            self.frame_traceback.pack(fill='both', padx=4, pady=(4, 4))
        if report_msg:
            self.frame_report = ttk.Frame(self)
            self.create_report_msg(report_msg)
            self.frame_report.pack(fill="x", padx=4, pady=(4, 0))


    def create_traceback(self, message, traceback):
        l = len(message)
        l2 = len(traceback)
        w = max(1, min(max(l, l2), 50))
        if not l2 and l // w < 3:
            w = 35

        self.frame_traceback.columnconfigure(0, weight=1)
        self.frame_traceback.rowconfigure(0, weight=1)
        txt_frame = ttk.Frame(self.frame_traceback, style='txt.TFrame', relief='sunken',
                              borderwidth=1)
        error_msg = tk.Text(txt_frame, width=w, wrap='word', font="TkDefaultFont 10",
                            bg='white', height=8, highlightthickness=0)
        error_msg.bind("<Button-1>", lambda event: error_msg.focus_set())
        error_msg.insert('1.0', traceback)
        error_msg.configure(state="disabled")
        scrolly = Scrollbar(self.frame_traceback, orient='vertical',
                            command=error_msg.yview)
        scrolly.grid(row=0, column=1, sticky='ns')
        scrollx = Scrollbar(self.frame_traceback, orient='horizontal',
                            command=error_msg.xview)
        scrollx.grid(row=1, column=0, sticky='ew')
        error_msg.configure(yscrollcommand=scrolly.set,
                            xscrollcommand=scrollx.set)
        error_msg.pack(side='left', fill='both', expand=True)
        txt_frame.grid(row=0, column=0, sticky='ewsn')

    def create_report_msg(self, report_msg):
        ttk.Label(self.frame_report, text=_("Please report this bug on ")).pack(side="left")
        url = ttk.Label(self.frame_report, style="url.TLabel", cursor="hand1",
                        font="TkDefaultFont 10 underline",
                        text=REPORT_URL)
        url.pack(side="left")
        url.bind("<Button-1>", lambda e: url_open(REPORT_URL))


class TwoButtonBox(BaseButtonBox):
    """Messagebox with two buttons."""

    def __init__(self, parent, title="", message="", button1=_("Yes"),
                 button2=_("No"), image=None):
        """
        Create a messagebox with two buttons.

        Arguments:
            parent: parent of the toplevel window
            title: message box title
            message: message box text
            button1/2: message displayed on the first/second button
            image: image displayed at the left of the message
        """

        BaseButtonBox.__init__(self, parent, button1, button2, title=title,
                               message=message, image=image)


class AuthBox(BaseButtonBox):
    """Messagebox with two buttons."""

    def __init__(self, parent, title=_("Password"), message=""):
        """
        Create a authentication messagebox.

        Arguments:
            parent: parent of the toplevel window
        """
        BaseButtonBox.__init__(self, parent, _("Unlock"), _("Cancel"),
                               title=_("Password"),
                               message=message, image='img_lock')
        self.result = None

        self.frame_pwd = ttk.Frame(self)
        ttk.Label(self.frame_pwd, text=_('Password:')).pack(side='left')
        self.entry_pwd = ttk.Entry(self.frame_pwd, show='*', justify='center')
        self.entry_pwd.pack(fill='x', expand=True, side='left', padx=(4, 0))
        self.entry_pwd.bind('<Return>', lambda ev: self.command(_("Unlock")))
        self.frame_pwd.pack(fill='x', expand=True, pady=4, padx=10)

        self.entry_pwd.focus_set()

    def command(self, name):
        if name == _("Unlock"):
            self.result = self.entry_pwd.get()
        self.destroy()


class AskYesNoCancel(BaseButtonBox):
    """Messagebox with two buttons."""

    def __init__(self, parent, title="", message="", image=None):
        """
        Create a messagebox with three buttons.

        Arguments:
            parent: parent of the toplevel window
            title: message box title
            message: message box text
            button1/2: message displayed on the first/second button
            image: image displayed at the left of the message
        """
        BaseButtonBox.__init__(self, parent, _("Yes"), _("No"), _("Cancel"),
                               title=title, message=message, image=image)

    def get_result(self):
        if self.result in [_("Cancel"), ""]:
            return None
        return self.result == _("Yes")


def showmessage(title="", message="", parent=None, button=_("Ok"), image=None):
    """
    Display a dialog with a single button.

    Return the text of the button ("Ok" by default)

    Arguments:
        title: dialog title
        message: message displayed in the dialog
        parent: parent window
        button: text displayed on the button
        image: image to display on the left of the message, either a PhotoImage
               or a string ('information', 'error', 'question', 'warning' or
               image path)
    """
    box = OneButtonBox(parent, title, message, button, image)
    box.wait_window(box)
    return box.get_result()


def showerror(title="", message="", traceback="", report_msg=False, parent=None):
    """
    Display an error dialog.

    Return "Ok"

    Arguments:
        title: dialog title
        message: message displayed in the dialog
        traceback: error traceback to display below the error message
        report_msg: if True display a suggestion to report error
        parent: parent window
    """
    box = ShowError(parent, title, message, traceback, report_msg)
    box.wait_window(box)
    return box.get_result()


def showinfo(title="", message="", parent=None):
    """
    Display an information dialog with a single button.

    Return "Ok".

    Arguments:
        title: dialog title
        message: message displayed in the dialog
        parent: parent window
    """
    return showmessage(title, message, parent, image="information")


def askokcancel(title="", message="", parent=None, icon=None):
    """
    Display a dialog with buttons "Ok" and "Cancel".

    Return True if "Ok" is selected, False otherwise.

    Arguments:
        title: dialog title
        message: message displayed in the dialog
        parent: parent window
        icon: icon to display on the left of the message, either a PhotoImage
              or a string ('information', 'error', 'question', 'warning' or
               mage path)
    """
    if icon is None:
        icon = "question"
    box = TwoButtonBox(parent, title, message, _("Ok"), _("Cancel"), icon)
    box.wait_window(box)
    return box.get_result() == _("Ok")


def askyesnocancel(title="", message="", parent=None, icon=None):
    """
    Display a dialog with buttons "Yes","No" and "Cancel".

    Return True if "Ok" is selected, False if "No" is selected, None otherwise.

    Arguments:
        title: dialog title
        message: message displayed in the dialog
        parent: parent window
        icon: icon to display on the left of the message, either a PhotoImage
              or a string ('information', 'error', 'question', 'warning' or
               mage path)
    """
    if icon is None:
        icon = "question"
    box = AskYesNoCancel(parent, title, message, image=icon)
    box.wait_window(box)
    return box.get_result()


def askoptions(title="", message="", parent=None, icon="question", *buttons):
    """
    Display a dialog with N buttons.

    Return the text of the clicked button, "" otherwise.

    Arguments:
        title: dialog title
        message: message displayed in the dialog
        parent: parent window
        icon: icon to display on the left of the message, either a PhotoImage
              or a string ('information', 'error', 'question', 'warning' or
              image path)
        buttons: button text list
    """
    box = BaseButtonBox(parent, *buttons, title=title, message=message, image=icon)
    box.wait_window(box)
    return box.get_result()


def askpassword(title=_("Password"), message="", parent=None):
    """
    Authentication box.

    Return the password, or None if cancelled.

    Arguments:
        title: dialog title
        message: message displayed in the dialog
        parent: parent window
    """
    box = AuthBox(parent, title=title, message=message)
    box.wait_window(box)
    return box.get_result()
