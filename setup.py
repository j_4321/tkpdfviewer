#! /usr/bin/python
# -*- coding:Utf-8 -*-

from setuptools import setup
from tkpdfviewerlib.constants import APP_NAME

import os
import sys

with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()


with open("tkpdfviewerlib/__init__.py") as file:
    exec(file.read())

if 'linux' in sys.platform:
    images = [os.path.join("tkpdfviewerlib/images/", img) for img in os.listdir("tkpdfviewerlib/images/")]
    data_files = [("/usr/share/applications", ["{}.desktop".format(APP_NAME)]),
                  ("/usr/share/{}/images/".format(APP_NAME), images),
                  ("/usr/share/doc/{}/".format(APP_NAME), ["README.rst", "changelog"]),
                  ("/usr/share/man/man1", ["{}.1.gz".format(APP_NAME)]),
                  ("/usr/share/pixmaps", ["{}.svg".format(APP_NAME)])]
    for loc in os.listdir('tkpdfviewerlib/locale'):
        data_files.append(("/usr/share/locale/{}/LC_MESSAGES/".format(loc),
                           ["tkpdfviewerlib/locale/{}/LC_MESSAGES/{}.mo".format(loc, APP_NAME)]))
    print(data_files)
    package_data = {}
else:
    data_files = []
    data = ['images/*']
    for loc in os.listdir('tkpdfviewerlib/locale'):
        data.append("tkpdfviewerlib/locale/{}/LC_MESSAGES/{}.mo".format(loc, APP_NAME))
    package_data = {'tkpdfviewerlib': data}


setup(name=APP_NAME,
      version=__version__,
      description="Basic PDF viewer",
      author="Juliette Monsel",
      author_email="j_4321@protonmail.com",
      license="GPLv3",
      url="https://github.com/j4321/{}/".format(APP_NAME),
      packages=['tkpdfviewerlib'],
      scripts=[APP_NAME],
      data_files=data_files,
      package_data=package_data,
      classifiers=[
              'Development Status :: 5 - Production/Stable',
              # 'Intended Audience :: Developers',
              # 'Intended Audience :: End Users/Desktop',
              # 'Operating System :: OS Independent',
              # 'Topic :: ...',
              'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
              'Programming Language :: Python :: 3',
              'Programming Language :: Python :: 3.5',
              'Programming Language :: Python :: 3.6',
              'Natural Language :: English',
              'Natural Language :: French'
      ],
      long_description=long_description,
      install_requires=['pymupdf', 'Pillow'])
