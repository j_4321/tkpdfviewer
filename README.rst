tkPDFViewer - Basic PDF viewer
==============================
|Linux| |License|

PDF viewer for Linux based on PyMuPDF and tkinter.

Features:
    - Read-only display of annotations (including popups)
    - Zoom


Install
-------

.. ~ - Archlinux

.. ~     tkPDFViewer is available in `AUR <https://aur.archlinux.org/packages/tkPDFViewer>`_.

.. ~ - Ubuntu

.. ~     tkPDFViewer is available in the PPA `ppa:j-4321-i/ppa <https://launchpad.net/~j-4321-i/+archive/ubuntu/ppa>`_.

.. ~     ::

.. ~         $ sudo add-apt-repository ppa:j-4321-i/ppa
.. ~         $ sudo apt-get update
.. ~         $ sudo apt-get install tkPDFViewer

- Source code

    First, install the missing dependencies among:

        - PyMuPDF
        - Pillow

    Then install the application:

    ::

        $ sudo python3 setup.py install


You can now launch it from *Menu > Office > tkPDFViewer*. You can launch
it from the command line with ``tkPDFViewer``.


Troubleshooting
---------------

If you encounter bugs or if you have suggestions, please open an issue
on `Gitlab <https://gitlab.com/j_4321/tkPDFViewer/issues>`_.

.. ~ .. |Release| image:: https://badge.fury.io/gh/j4321%2FtkPDFViewer.svg
.. ~     :alt: Latest Release
.. ~     :target: https://badge.fury.io/gh/j4321%2FtkPDFViewer

.. |Linux| image:: https://img.shields.io/badge/platform-Linux-blue.svg
    :alt: Linux
    
.. |License| image:: https://img.shields.io/badge/licence-GPL3-orange.svg
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: License - GPLv3
