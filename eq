[33mcommit e991e9710d9af5b64bdefbe12b31980467d938be[m[33m ([m[1;36mHEAD -> [m[1;32medit[m[33m, [m[1;31morigin/edit[m[33m)[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Thu Feb 18 19:38:31 2021 +0100

    Add annot property edition

[33mcommit 4f7762b4c64d52d5a147b75f56cc8398c4d84734[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Thu Feb 18 17:04:44 2021 +0100

    Fix support for Ink annot

[33mcommit ed86c09ba08224cc8a237c7d95fd43c78e25f03f[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Thu Feb 18 16:59:45 2021 +0100

    Add reload button

[33mcommit fc86e82f88973a21c44805e455cb5d0fc56b6b5c[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Thu Feb 18 16:18:52 2021 +0100

    Implement annot deletion

[33mcommit 265ec2ac3088d62c5fce42adbaf987aa6c518f1e[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Feb 17 18:17:16 2021 +0100

    Improve annots
    
    - Make notes editable
    - Add possibility to create rectangle, circle, text and line annots (freetext is WIP)

[33mcommit 8de4ab2ccbee2db5094b5a88f316890f41eb00d7[m[33m ([m[1;31morigin/search[m[33m, [m[1;31morigin/master[m[33m, [m[1;32msearch[m[33m, [m[1;32mmaster[m[33m)[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Feb 15 10:00:38 2021 +0100

    Add compatibility with newer pymupdf version

[33mcommit f3465e3ac2a67529b61061fdb4870db8d3e7fbd2[m[33m ([m[1;31morigin/size[m[33m)[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Fri Nov 13 10:41:46 2020 +0100

    Fix page size (conversion pt to px)

[33mcommit 8b7a7fbdfcddd91e20792b3d3cedcd6130e76c98[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Thu Nov 5 10:11:48 2020 +0100

    Make sure current search result is visible during search

[33mcommit 32184fd96ecf7383d5f5a9d5909af31b77a40eea[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 18:49:29 2020 +0100

    Add toggle button for left pane

[33mcommit df9d653e3502ef538c5dd31e842e4fe68c495775[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 18:19:27 2020 +0100

    Fix links

[33mcommit 0870f35b4a334042b55dc2a667ad893fa4881811[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 18:11:51 2020 +0100

    Add tooltips and thumbnails

[33mcommit dbf061404c025f1a781c963ddf624bd776c644b4[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 15:28:48 2020 +0100

    Add basic latex rendering of annotations

[33mcommit 4742132909f848e57421f6ce93ea87f6b6c8b6b2[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 14:47:44 2020 +0100

    Improve popup style

[33mcommit 3917d895a7747a5d373ae0e26d43a09abf872d2d[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 12:23:51 2020 +0100

    Add support for encrypted pdf

[33mcommit ec1f550d819217cfba51e64f8517f5aaa195e86e[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 12:23:27 2020 +0100

    Improve messageboxes

[33mcommit e3ca49d52f72bf7d3bf39f1196ad9039639c9f68[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Wed Nov 4 09:37:42 2020 +0100

    Add save and saveas functions

[33mcommit 6322af74e3795674b2a252b66e8c35aa2f8f6cb4[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Tue Nov 3 13:54:43 2020 +0100

    Add search bar

[33mcommit 68a72da3cdd68ef313e8b40a3096ec4c0eda5ed5[m[33m ([m[1;31morigin/dev[m[33m)[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 21:46:25 2020 +0100

    Add support for hyperlinks

[33mcommit 70d75abf4f9f8e434d0976eaaaeadf6de321221a[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 21:17:21 2020 +0100

    Add entry to navigate between pages

[33mcommit 07ac53090da2379d798f2af7eb789d7a173bb91c[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 21:09:23 2020 +0100

    Clean repo and add .gitignore

[33mcommit e2cd374795fac02bedffc79a93f0c93391caf6c1[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 20:57:10 2020 +0100

    Update README

[33mcommit 4c7d3042898caba2a782f0d1f0c604045b53de13[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 20:56:28 2020 +0100

    Update README

[33mcommit aaaeb360ddaba535ecbec033ffb93cd2dbab4053[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 20:51:29 2020 +0100

    Update README

[33mcommit 26a9281891ff3c0610af63a54f61ee572c77122c[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 20:44:31 2020 +0100

    Add support readonly annotations

[33mcommit b71de4a3074ce952fb4a6549df8c8b5adf0613c6[m
Author: Juliette Monsel <j_4321@protonmail.com>
Date:   Mon Nov 2 16:23:36 2020 +0100

    Initial commit
